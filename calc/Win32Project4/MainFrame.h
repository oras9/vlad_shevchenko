#pragma once
#include <wx/wx.h>

class MainFrame : public wxFrame
{
public:
	MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size);

	wxTextCtrl* display;
	wxBoxSizer* vBoxSizer;
	wxGridSizer* gridSizer;

	wxString* firstOperand;
	wxString* calcOperator;
	wxString* secondOperand;
	wxString* thirdOperand;
	bool isSecondOperator;

	void OnOneButtonClick(wxCommandEvent& commandEvent);
	void OnTwoButtonClick(wxCommandEvent& commandEvent);
	void OnThreeButtonClick(wxCommandEvent& commandEvent);
	void OnMinusButtonClick(wxCommandEvent& commandEvent);
	void OnPlusButtonClick(wxCommandEvent& commandEvent);
	void OnEqualButtonClick(wxCommandEvent& command);
	void OnClsButtonClick(wxCommandEvent& command);
	void OnDivisionButtonClick(wxCommandEvent& command);
	void OnFourButtonClick(wxCommandEvent& command);
	void OnMultiplicationButtonClick(wxCommandEvent& command);
	void OnFiveButtonClick(wxCommandEvent& command);
	void OnSixButtonClick(wxCommandEvent& command);
	void OnSevenButtonClick(wxCommandEvent& command);
	void OnEightButtonClick(wxCommandEvent& command);
	void OnNineButtonClick(wxCommandEvent& command);


	DECLARE_EVENT_TABLE();


};